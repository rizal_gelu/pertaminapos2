package com.example.myfirstapp;


import com.example.myfirstapp.model.api.RegistResponse;
import com.example.myfirstapp.model.api.ResponseAktaPerusahan;
import com.example.myfirstapp.model.api.LoginData;
import com.example.myfirstapp.model.api.ResponseCasting.SecurityData;
import com.example.myfirstapp.model.api.SecurityResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;

import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {

    @FormUrlEncoded
    @POST("login")
    Call<LoginData> callApiLogin(@Field("email") String email, @Field("password") String Password);

    @FormUrlEncoded
    @POST("register")
    Call<RegistResponse> callApiRegis(@Field("email") String email,
                                     @Field("password") String Password,
                                     @Field("id_security_question") String id_security_question,
                                     @Field("answer") String answer);

    @FormUrlEncoded
    @GET("akta_perusahan")
    Call<ResponseAktaPerusahan> callApiGetAkta(@Query("email") String email);

    @FormUrlEncoded
    @POST("akta_perusahan")
    Call<ResponseAktaPerusahan> callApiPostAkta(@Field("email") String email,
                                                @Field("distributor_name") String distributor_name,
                                                @Field("no_akta_perusahan") String no_akta_perusahan,
                                                @Field("jenis_akta_perusahan") String jenis_akta_perusahan,
                                                @Field("tanggal_akta_berlaku") String tanggal_akta_berlaku,
                                                @Field("akta_pengesahan_kehakiman") String akta_pengesahan_kehakiman);

    @GET("security_question")
    Call<SecurityResponse> callApiSecurity();

}
