package com.example.myfirstapp.model.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RegisterData implements Serializable {
	private String password;
	private String answer;
	@SerializedName("id_security_question")
	private String id_security_question;
	private String email;

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setAnswer(String answer){
		this.answer = answer;
	}

	public String getAnswer(){
		return answer;
	}

	public void setIdSecurityQuestion(String id_security_question){
		this.id_security_question = id_security_question;
	}

	public String getIdSecurityQuestion(){
		return id_security_question;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

//	@Override
// 	public String toString(){
//		return
//			"RegisterData{" +
//			"password = '" + password + '\'' +
//			",answer = '" + answer + '\'' +
//			",id_security_question = '" + idSecurityQuestion + '\'' +
//			",email = '" + email + '\'' +
//			"}";
//		}
}
