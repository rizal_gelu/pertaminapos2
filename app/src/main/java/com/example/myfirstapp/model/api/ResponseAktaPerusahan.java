package com.example.myfirstapp.model.api;

import com.example.myfirstapp.model.api.ResponseCasting.AktaPerusahan;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseAktaPerusahan implements Serializable {

    public ResponseAktaPerusahan(){

    }

    @SerializedName("message")
    private String message;

    @SerializedName("code")
    private int code;

    @SerializedName("data")
    private List<AktaPerusahan> data = new ArrayList<>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ArrayList<AktaPerusahan> getData() {
        return (ArrayList<AktaPerusahan>) data;
    }

    public void setData(List<AktaPerusahan> data) {
        this.data = data;
    }
}
