package com.example.myfirstapp.model.api.ResponseCasting;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AktaPerusahan implements Serializable {

    public AktaPerusahan() {

    }

    @SerializedName("distributor_name")
    private  String distributor_name;
    @SerializedName("no_akta_perusahan")
    private  String no_akta_perusahan;
    @SerializedName("jenis_akta_perusahan")
    private String jenis_akta_perusahan;
    @SerializedName("tanggal_akta_berlaku")
    private String tanggal_akta_berlaku;
    @SerializedName("akta_pengesahan_kehakiman")
    private String akta_pengesahan_kehakiman;
    @SerializedName("file_akta")
    private String file_akta;

    public String getDistributor_name() {
        return distributor_name;
    }

    public void setDistributor_name(String distributor_name) {
        this.distributor_name = distributor_name;
    }

    public String getNo_akta_perusahan() {
        return no_akta_perusahan;
    }

    public void setNo_akta_perusahan(String no_akta_perusahan) {
        this.no_akta_perusahan = no_akta_perusahan;
    }

    public String getJenis_akta_perusahan() {
        return jenis_akta_perusahan;
    }

    public void setJenis_akta_perusahan(String jenis_akta_perusahan) {
        this.jenis_akta_perusahan = jenis_akta_perusahan;
    }

    public String getTanggal_akta_berlaku() {
        return tanggal_akta_berlaku;
    }

    public void setTanggal_akta_berlaku(String tanggal_akta_berlaku) {
        this.tanggal_akta_berlaku = tanggal_akta_berlaku;
    }

    public String getAkta_pengesahan_kehakiman() {
        return akta_pengesahan_kehakiman;
    }

    public void setAkta_pengesahan_kehakiman(String akta_pengesahan_kehakiman) {
        this.akta_pengesahan_kehakiman = akta_pengesahan_kehakiman;
    }

    public String getFile_akta() {
        return file_akta;
    }

    public void setFile_akta(String file_akta) {
        this.file_akta = file_akta;
    }
}
