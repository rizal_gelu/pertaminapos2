package com.example.myfirstapp.model.api.ResponseCasting;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SecurityData implements Serializable {

    @SerializedName("created_by")
    private String createBy;
    @SerializedName("date_updated")
    private String dateUpdated;
    private String question;
    @SerializedName("date_created")
    private String dateCreated;
    @SerializedName("id_security_question")
    private String idSecurityQuestion;

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setDateUpdated(String dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getDateUpdated() {
        return dateUpdated;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestion() {
        return question;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setIdSecurityQuestion(String idSecurityQuestion) {
        this.idSecurityQuestion = idSecurityQuestion;
    }

    public String getIdSecurityQuestion() {
        return idSecurityQuestion;
    }

}

