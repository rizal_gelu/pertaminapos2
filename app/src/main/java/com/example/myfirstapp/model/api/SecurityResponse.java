package com.example.myfirstapp.model.api;

import com.example.myfirstapp.model.api.ResponseCasting.SecurityData;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SecurityResponse {
    private String code;
    private List<SecurityData> data;
    private String message;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setData(List<SecurityData> data) {
        this.data = data;
    }

    public List<SecurityData> getData() {
        return data;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }



}