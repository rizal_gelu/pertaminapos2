package com.example.myfirstapp.presenter.manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {

    public static final String TAG = "Preferences";
    private Context mContext;

    public static SharedPreferences mSharedPreferences;

    public static final String IS_USER_LOGIN = "is_user_login";

}
