package com.example.myfirstapp.view.auth;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.ApiClient;
import com.example.myfirstapp.ApiService;
import com.example.myfirstapp.R;

import com.example.myfirstapp.model.api.LoginData;
import com.example.myfirstapp.presenter.manager.SessionManagement;
import com.example.myfirstapp.view.dialog.ForgotPasswordDialog;
import com.example.myfirstapp.view.legal.AktaPerusahaanActivity;
import com.example.myfirstapp.view.personal.PersonalInfoActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    EditText txt_email;
    EditText txt_pw;
    Button btn_login;
    TextView tv_forgot;
    TextView tv_regist;

    ProgressDialog loading;
    Context mContext;
    ApiService mApiService;

    SessionManagement session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = this;
        mApiService = ApiClient.getClient().create(ApiService.class);
        session = new SessionManagement(mContext);
        

        txt_email = findViewById(R.id.txt_email);
        txt_pw = findViewById(R.id.txt_pw);
        btn_login = findViewById(R.id.btn_login);
        tv_forgot = findViewById(R.id.tv_forgot_pass);
        tv_regist = findViewById(R.id.tv_regist);


        tv_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestLogin();
            }
        });
    }

    private void requestLogin() {
        mApiService.callApiLogin(txt_email.getText().toString(), txt_pw.getText().toString())
                .enqueue(new Callback<LoginData>() {
                    @Override
                    public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                        if (response.isSuccessful()) {
                            session.createLoginSession("is Logged_in", txt_email.getText().toString(), response.body().getToken());

                            Intent intent = new Intent(LoginActivity.this, AktaPerusahaanActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(mContext, "Login Gagal", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginData> call, Throwable t) {

                    }
                });
    }
}






