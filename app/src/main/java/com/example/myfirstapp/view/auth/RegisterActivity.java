package com.example.myfirstapp.view.auth;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.ApiClient;
import com.example.myfirstapp.ApiService;
import com.example.myfirstapp.R;
import com.example.myfirstapp.model.api.RegistResponse;
import com.example.myfirstapp.model.api.RegisterData;
import com.example.myfirstapp.model.api.ResponseCasting.SecurityData;
import com.example.myfirstapp.model.api.SecurityResponse;
import com.example.myfirstapp.view.formValidation.FormValidation;
import com.example.myfirstapp.view.legal.AktaPerusahaanActivity;
import com.example.myfirstapp.view.personal.PersonalInfoActivity;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    EditText txt_email, txt_pw,txt_rpw, txt_answer;

    Button btn_register;
    TextView tv_login;
    Spinner sec_question;
    Context mContext;

    private boolean isSignUpFormEnable = true;
    private boolean isFormValidationSuccess = false;
    ProgressDialog loading;
    ApiService mApiService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mApiService = ApiClient.getClient().create(ApiService.class);
        mContext = this;

        initComponent();
        initSpinner();
    }

    public void initComponent() {
        txt_email = findViewById(R.id.txt_email);
        txt_pw = findViewById(R.id.txt_pw);
        txt_rpw = findViewById(R.id.txt_rpw);
        btn_register = findViewById(R.id.btn_register);
        tv_login = findViewById(R.id.tv_login);
        sec_question = findViewById(R.id.sec_question);
        txt_answer = findViewById(R.id.txt_answer);

        addTextWatcher(txt_email);
        addTextWatcher(txt_pw);
        addTextWatcher(txt_rpw);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFormValidationSuccess) {
                    requestRegister();
                } else {
                    Toast.makeText(RegisterActivity.this, "Password Anda Tidak Sama", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void initSpinner(){

            mApiService.callApiSecurity().enqueue(new Callback<SecurityResponse>() {
                @Override
                public void onResponse(Call<SecurityResponse> call, Response<SecurityResponse> response) {
                    if (response.isSuccessful()) {

                        List<SecurityData> semuaItems = response.body().getData();
                        List<String> listSpinner = new ArrayList<String>();
                        for (int i = 0; i < semuaItems.size(); i++) {
                            listSpinner.add(semuaItems.get(i).getQuestion());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                R.layout.spinner_item, listSpinner);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sec_question.setAdapter(adapter);

                    } else {

                        Toast.makeText(mContext, "Gagal mengambil data", Toast.LENGTH_SHORT).show();
                    }
                }


                @Override
                public void onFailure(Call<SecurityResponse> call, Throwable t) {

//                Toast.makeText(mContext, "Koneksi internet bermasalah", Toast.LENGTH_SHORT).show();
                }
            });

            sec_question.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    String selectedName = parent.getItemAtPosition(position).toString();
                    Toast.makeText(mContext, "Kamu memilih " + selectedName, Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }



    public void login(View v) {
        Intent i = new Intent();
        i.setClass(this, LoginActivity.class);
        startActivity(i);
    }


    private void validateForm() {
        String pass = txt_pw.getText().toString();
        String pass2 = txt_rpw.getText().toString();

        if (FormValidation.required(pass) && FormValidation.validPass(pass)
                && FormValidation.required(pass2) && FormValidation.validPass(pass2) && (pass.equals(pass2))) {
            isFormValidationSuccess = true;
        } else {
            isFormValidationSuccess = false;
        }
    }

    public void requestRegister() {
        String id_security_question = "460a9eeb-aa90-5a57-a735-05825734";
        mApiService.callApiRegis(txt_email.getText().toString(), txt_pw.getText().toString(), id_security_question, txt_answer.getText().toString()).enqueue(new Callback<RegistResponse>() {
            @Override
            public void onResponse(Call<RegistResponse> call, Response<RegistResponse> response) {
                if (response.code() == 200){
                    Intent intent = new Intent(RegisterActivity.this, PersonalInfoActivity.class);
                    startActivity(intent);

                }

            }

            @Override
            public void onFailure(Call<RegistResponse> call, Throwable t) {

            }
        });
    }

        public void addTextWatcher(final EditText input) {
        input.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (isSignUpFormEnable) {
                    validateForm();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
    }
}
