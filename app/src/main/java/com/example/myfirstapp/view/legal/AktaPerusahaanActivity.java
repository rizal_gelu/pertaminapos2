package com.example.myfirstapp.view.legal;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myfirstapp.ApiClient;
import com.example.myfirstapp.ApiService;
import com.example.myfirstapp.R;
import com.example.myfirstapp.model.api.ResponseAktaPerusahan;
import com.example.myfirstapp.model.api.ResponseCasting.AktaPerusahan;
import com.example.myfirstapp.presenter.manager.SessionManagement;
import com.example.myfirstapp.view.personal.ContactActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AktaPerusahaanActivity extends AppCompatActivity {

    EditText txt_distributor;
    EditText txt_no_akta;
    EditText txt_jenis_akta;
    EditText txt_tgl_akta;
    EditText txt_akta_pengesahan;
    Button btn_back;
    Button btn_next;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    DatePickerDialog.OnDateSetListener dates;
    ApiService mApiService;
    Context mContext;
    SessionManagement session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akta);
        mApiService = ApiClient.getClient().create(ApiService.class);

        initView();
        initContent();
    }


    public void initView() {
        txt_distributor = findViewById(R.id.txt_distributor);
        txt_no_akta = findViewById(R.id.txt_no_akta);
        txt_jenis_akta = findViewById(R.id.txt_jenis_akta);
        txt_tgl_akta = findViewById(R.id.txt_tgl_akta);
        txt_akta_pengesahan = findViewById(R.id.txt_akta_pengesahan);
        btn_back = findViewById(R.id.btn_back);
        btn_next = findViewById(R.id.btn_next);
    }

    public void initContent() {
        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();

            }
        };

        dates = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabels();

            }
        };

        txt_tgl_akta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AktaPerusahaanActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        txt_akta_pengesahan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AktaPerusahaanActivity.this, dates, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = "rizalgelui@yahoo.co.id";
                session = new SessionManagement(getApplicationContext());
                session.checkLogin();
                HashMap<String, String> user = session.getUserDetails();
//                String email = user.get(SessionManagement.KEY_EMAIL);
                mApiService.callApiPostAkta(email, txt_distributor.getText().toString(),
                        txt_no_akta.getText().toString(), txt_jenis_akta.getText().toString(), txt_tgl_akta.getText().toString(),
                        txt_akta_pengesahan.getText().toString()).enqueue(new Callback<ResponseAktaPerusahan>() {
                    @Override
                    public void onResponse(Call<ResponseAktaPerusahan> call, Response<ResponseAktaPerusahan> response) {
                        if (response.isSuccessful()) {
                            Intent intent = new Intent(AktaPerusahaanActivity.this, NPWPActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(mContext, "Login Gagal", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseAktaPerusahan> call, Throwable t) {

                    }
                });
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AktaPerusahaanActivity.this, ContactActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        txt_tgl_akta.setText(sdf.format(myCalendar.getTime()));

    }

    private void updateLabels() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        txt_akta_pengesahan.setText(sdf.format(myCalendar.getTime()));
    }

    public void checkingDataAkta() {
        session = new SessionManagement(getApplicationContext());
        session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();
        String email = user.get(SessionManagement.KEY_EMAIL);
        mApiService.callApiGetAkta(email).enqueue(new Callback<ResponseAktaPerusahan>() {
            @Override
            public void onResponse(Call<ResponseAktaPerusahan> call, Response<ResponseAktaPerusahan> response) {
                ResponseAktaPerusahan responseAktaPerusahan = response.body();
                List<AktaPerusahan> data = responseAktaPerusahan.getData();
                System.out.println(data);
            }

            @Override
            public void onFailure(Call<ResponseAktaPerusahan> call, Throwable t) {

            }
        });
    }

}

