package com.example.myfirstapp.view.personal;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


import com.example.myfirstapp.R;


public class CompletedPersonalActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_complete);
    }
}
